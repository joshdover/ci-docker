FROM python:2.7.7

# Main deps
RUN curl -sL https://deb.nodesource.com/setup_5.x | bash -
RUN apt-get update -y && apt-get install -y nginx libmemcached-dev libffi-dev libffi6 libxml2 libxml2-dev nodejs libjpeg-dev libjpeg8 libmagic1 tmux rubygems bc memcached postgresql-9.4 redis-server openssl nodejs
RUN gem install foreman
RUN npm install -g gulp-cli bower
RUN pip install virtualenv

# Configure postgres
RUN service postgresql start && pg_dropcluster --stop 9.4 main && pg_createcluster --start -e UTF-8 9.4 main && su -l postgres -c "createuser -sdr root" && echo "CREATE DATABASE cratejoy_test WITH ENCODING 'UTF8';" | psql postgres
COPY pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
